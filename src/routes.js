import React from "react";
import { Route, Routes } from "react-router-dom";
import routesConfig from "./routesConfig"

const RouterComponent = () => {
  return (
      <Routes>
        {routesConfig.map((it,index) => <Route key={index} path={it.path} element={it.component}/>)}
      </Routes>
  );
};
export default RouterComponent;
