export const sortBy = (field , type) => {
    return type === "most" ? (a,b) => b[field] - a[field] : (a,b) => a[field] - b[field]

}

export const filterBy = (field , type) => {
    return (it) => it[field] === type
}