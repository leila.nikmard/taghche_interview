import React, { useEffect, useState, useCallback } from "react";
import BookBox from "../../components/Book/BookBox";
import { sortBy , filterBy } from "./util";
import SortSelector from "../../components/SortSelector";
import FilterSelector from "../../components/FilterSelector";
import Loading from "../../components/Loading";
import { loadAllBooks } from "../../utils/services";
import Error from "../../components/Statuses/Error";
import { Virtuoso } from 'react-virtuoso';

import "./style.scss"

const HomePage = () => {

    const [bookList , setBookList] = useState([]);
    const [loading , setLoading] = useState(false);
    const [refreshing , setRefreshing] = useState(false);
    const [sortSelect , setSortSelect] = useState("");
    const [filterSelect , setFilterSelect] = useState("");
    const [errorMessage , setErrorMessage] = useState("");
    const [offset , setOffset] = useState("16");

    const filter = encodeURI(`{"list":[{"type":3,"value":164},{"type":21,"value":0},{"type":50,"value":0}]}`);

    const handleLoadAllBooks = async () => {
        setLoading(true);
        setOffset(+offset+16)
        try{
            const response = await loadAllBooks(filter,offset);
            localStorage.setItem("offset" , offset)
            setLoading(false);
            setRefreshing(false);
            const booksListData = response?.data?.bookList?.books;
            setBookList([...bookList ,...booksListData])

        } catch(error){
            setErrorMessage(error.message);
        }
    }

    const loadMore = useCallback(() => {
        return handleLoadAllBooks()
    }, [bookList]);

    useEffect(() => {
        setRefreshing(true);
        loadMore();
    },[]);

    const handleSortSelect = (e) => {
        const value = e.target.value;
        setSortSelect(value);
        switch(value) {
            case "leastPrice":
              var data = bookList.sort(sortBy("price" , "least"));
              setBookList(data)
              break;
            case "leastRating":
                var data = bookList.sort(sortBy("rating" , "least"));
                setBookList(data)
              break;
              case "mostRating":
                var data = bookList.sort(sortBy("rating" , "most"));
                setBookList(data)
              break;
              case "mostPrice":
                var data = bookList.sort(sortBy("price" , "most"));
                setBookList(data)
              break;
            default:
               return bookList
          }
    }

    const handleFilterSelect = (e) => {
        const value = e.target.value;
        setFilterSelect(value);
        setRefreshing(true);
        loadAllBooks(filter,offset)
        .then((response => {
            setRefreshing(false);
            const books = response?.data?.bookList?.books;
            if(value){
                const filterBooks = books?.filter(filterBy("type", value));
                setBookList(filterBooks)
            }else{
                setBookList(books)
            }
        }))
    };
       
    if(errorMessage){
        return <Error message={errorMessage}/>
    }

    if(refreshing){
        return (
        <div className="container">
            <div className="selects-container">
                <SortSelector value={sortSelect} handleSortSelect={handleSortSelect}/>
                <FilterSelector value={filterSelect} handleFilterSelect={handleFilterSelect}/>
            </div>
            <Loading />
        </div>)
    }

    return(
        <div className="container">
            <div className="selects-container">
                <SortSelector value={sortSelect} handleSortSelect={handleSortSelect}/>
                <FilterSelector value={filterSelect} handleFilterSelect={handleFilterSelect}/>
            </div>
            {bookList?.length > 0 ? 
            <>
                <Virtuoso
                    style={{ height: "100vh" }}
                    useWindowScroll
                    data={bookList}
                    endReached={loadMore}
                    itemContent={(index, item) => (
                        <div className="book-container">
                            <BookBox data={item}/>
                        </div>
                    )}
                />
                {loading && <p>در حال اپلود</p>}
            </> : 
            <p className="no-data">نتیجه یافت نشد.</p>}
        </div>
    )
}

export default HomePage;