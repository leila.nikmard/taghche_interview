import React, { useEffect, useState } from "react";
import { useParams } from 'react-router-dom';
import BookDetail from "../../components/Book/BookDetail"
import Loading from "../../components/Loading";
import { loadAllBooks } from "../../utils/services";
import Error from "../../components/Statuses/Error";

const Detail = () => {
    
    const [detailBook , setDetailBook] = useState({});
    const [errorMessage , setErrorMessage] = useState("");
    const [loading,setLoading] = useState(false);
    const [offset , setOffset] = useState(localStorage.getItem("offset"));

    const filter = encodeURI(`{"list":[{"type":3,"value":164},{"type":21,"value":0},{"type":50,"value":0}]}`);
    const params = useParams();

    useEffect(() => {
        handleLoadBook()
    },[]);

    const handleLoadBook = async () => {
        setLoading(true);
        try{
            const response = await loadAllBooks(filter,offset);
            setLoading(false)
            const booksData = response?.data?.bookList?.books;
            const data = booksData.find(it => it.id == params["id"]);
            setDetailBook(data);
        } catch(error){
            setErrorMessage(error.message);
        }
    }

    if(errorMessage){
        return <Error message={errorMessage}/>
    }

    return(
        <div>
            {loading ? <div><Loading/></div> : <BookDetail data={detailBook}/>}
        </div>
    )
}

export default Detail;