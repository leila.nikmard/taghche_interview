import Home from "./pages/Home";
import Detail from "./pages/Detail";

const routesConfig = [
    {
        path:"/",
        component: <Home/>
    },
    {
        path:"/book/:id",
        component: <Detail/>
    },
];

export default routesConfig;

