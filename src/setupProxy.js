const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/everything',
    createProxyMiddleware({
      target: "https://get.taaghche.com/v2",
      secure: false,
      changeOrigin: true,
      
    })
  );
};