import RouterComponent from "./routes";
import NoInternetConnection from "./components/Statuses/NoInternetConnection ";

import './App.css';

function App() {
  
  return (
    <div className="App">
    <NoInternetConnection>
      <RouterComponent />
    </NoInternetConnection>
    </div>
  );
}

export default App;
