import React from "react";

import "./style.scss";

const BookDetail = ({data}) => {
    return (
        <div className="detail">
            <div className="detail-cover">
                <img src={data?.coverUri} widt="100%" height="100%" alt={data?.title}/>
            </div>
            <div className="detail-info">
                <h1 className="detail-info__title">{data?.title}</h1>
                <p className="detail-info__authors">
                    <span>{data?.authors?.length > 1 ? "نویسندگان" : "نویسنده"}</span>
                    <span>
                        {data?.authors?.map(it => (
                            <span key={it?.id}>{it?.slug}{" "}</span>
                        ))}
                    </span>
                </p>
                <p className="detail-info__price">
                    <span>قیمت</span>
                    <span>{data?.price}</span>
                </p>
                <p className="detail-info__rating">
                    <span>امتیاز</span>
                    <span>{data?.rating && Math.floor(data?.rating)}</span>
                </p>
                <p className="detail-info__publisher">
                    <span>ناشر</span>
                    <span>{data?.publisher}</span>
                </p>
                <p className="detail-info__number">
                    <span>تعداد صفحات</span>
                    <span>{data?.numberOfPages}</span>
                </p>
                <p className="detail-info__price">
                    <span>قیمت فیزیکی</span>
                    <span>{data?.PhysicalPrice}</span>
                </p>
                <button className="add-to-cart">افزودن به سبد خرید</button>
            </div>
            <div className="description">
                <h2 className="description__title">توضیحات</h2>
                <p className="description__text">{data?.description}</p>
            </div>
        </div>
    )
}

export default BookDetail;