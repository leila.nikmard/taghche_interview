import React from "react";
import BookInfo from "../BookInfo";

import "./style.scss";

const BookBox  = ({data}) => {

    return(
        <>
            <div className="book">
                <div className="book-cover">
                    <img src={data?.coverUri} alt={data?.title} height="180px" width="120px"/>
                </div>
                <BookInfo data={data}/>
            </div>
        </>
    )
}

export default BookBox;