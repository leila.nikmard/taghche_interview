import React from "react";
import {Link} from "react-router-dom";
import { Rating } from 'react-simple-star-rating';

import "./style.scss";

const BookInfo = ({data}) => {
    return(
        <div className="book-info">
            <h4 className="book-info__title">{data?.title}</h4>
            <p className="book-info__authors">{data?.authors.map(it => (
                <span key={it?.id}>{it?.slug}</span>
            ))}</p>
            <p className="book-info__price">{data?.price}</p>
            <div className="book-info__rating">
                <Rating initialValue={data?.rating} size="15px"/>
            </div>
            <Link to={`/book/${data?.id}`}><button className="more-detail">جزئیات بیشتر</button></Link>
        </div>
    )
}

export default BookInfo;