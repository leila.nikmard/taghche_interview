import React from "react";
import Select from "../Form/Select";

import "./style.scss";

const SortSelector = ({value, handleSortSelect}) => {
    const options = [
        {id: "" , value:"انتخاب کنید"},
        {id: "mostPrice" , value:"بیشترین قیمت"},
        {id: "leastPrice" , value:"کمترین قیمت"},
        {id: "mostRating" , value:"بیشترین امتیاز"},
        {id: "leastRating" , value:"کمترین امتیاز"},
    ];

    return(
        <div className="sort-box">
            <span className="sort__title">مرتب سازی</span>
            <Select value={value} options={options} handleChange={handleSortSelect}/>
        </div>
    )
}

export default SortSelector;