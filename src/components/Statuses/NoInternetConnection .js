import React, {useState, useEffect} from 'react';

import "./style.scss"

const NoInternetConnection = ({children}) => {
    const [isOnline, setOnline] = useState(true);

    useEffect(()=>{
        setOnline(navigator.onLine)
    },[])

    window.addEventListener('online', () => {
        setOnline(true)
    });

    window.addEventListener('offline', () => {
        setOnline(false)
    });

    if(isOnline){
        return(
            children
        )
    }else {
        return(
            <h1 className='no-internet'>No Interner Connection, Please try again later.</h1>
        )
    }
}

export default NoInternetConnection;