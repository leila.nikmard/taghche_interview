import React from "react";

import "./style.scss"

const Error = ({message}) => {
    return(
        <h1 className="error">{message}</h1>
    )
}

export default Error;