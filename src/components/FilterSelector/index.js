import React from "react";
import Select from "../Form/Select";

import "./style.scss";

const FilterSelector = ({value, handleFilterSelect}) => {
    const options = [
        {id: "" , value:"انتخاب کنید"},
        {id: "Text" , value:"متنی"},
        {id: "Audio" , value:"صوتی"},
    ];

    return(
        <div className="filter-box">
            <span className="filter__title">فیلتر</span>
            <Select value={value} options={options} handleChange={handleFilterSelect}/>
        </div>
    )
}

export default FilterSelector;