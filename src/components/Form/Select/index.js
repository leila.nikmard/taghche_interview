import React from "react";

import "./style.scss";

const Select  = ({options,value, handleChange}) => {
    return(
        <select value={value} className="select" onChange={handleChange}>
           {options.map(it => (
                <option className="option" key={it?.id} value={it?.id}>{it?.value}</option>
           ))}
        </select>
    )
}

export default Select;